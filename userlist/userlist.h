/*
 * USERLIST - Configurable "rwho"-type replacement, shows only users online.
 * Header file
 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <pwd.h>

#define	BOOL	int

#define	TRUE	1
#define	FALSE	0

#define STRLEN	80
/* Display types */
#define	DISPLAY_CFINGERD	1
#define	DISPLAY_RFC1288		2
#define	DISPLAY_LINUX		3
#define	DISPLAY_GNU		4
#define	DISPLAY_BSDOS		5
#define	DISPLAY_UNIX5		6

typedef struct {
    char *username;
    char *tty;
    char *locale;
    char *line;
    long ip_addr;
    time_t time;
} TTY_FROM;

#include "config.h"

#ifdef SUNOS
#include <strings.h>
#include <utmpx.h>
#define utmp utmpx
#define ut_time ut_xtime
#define getutent getutxent
#define _PATH_UTMP UTMPX_FILE
#if (SUNOS == 55)
#define snprintf __snprintf
#endif
#else
#include <utmp.h>
#endif

#if !defined(UT_USERSIZE) ||  !defined(UT_HOSTSIZE) ||  !defined(UT_LINESIZE) ||  !defined(UT_NAMESIZE)
struct utmp foo_utmp;
#endif

#ifndef UT_USERSIZE
#define UT_USERSIZE sizeof(foo_utmp.ut_name)
#endif
#ifndef UT_HOSTSIZE
#define UT_HOSTSIZE sizeof(foo_utmp.ut_host)
#endif
#ifndef UT_LINESIZE
#define UT_LINESIZE sizeof(foo_utmp.ut_line)
#endif
#ifndef UT_NAMESIZE
#define UT_NAMESIZE sizeof(foo_utmp.ut_user)
#endif

extern TTY_FROM tty_list[MAX_TTYS];
extern int times_on, display_type;
extern int no_idle;
