 ####  ###### ###### ##  ##  ##### ###### #####  #####    ######  ####   ####
##  ## ##       ##   ### ## ##     ##     ##  ## ##  ##   ##     ##  ## ##  ##
##     ####     ##   ###### ## ### ####   #####  ##  ##   ####   ###### ##  ##
##  ## ##       ##   ## ### ##  ## ##     ## ##  ##  ##   ##     ##  ## ## ##
 ####  ##     ###### ##  ##  ##### ###### ##  ## #####    ##     ##  ##  ## ##

                                                The CFINGERD FAQ version 1.0.2
                                           by Ken Hollis <khollis@bitgate.com>
                                      Martin Schulze <joey@@infodrom.north.de>

* This FAQ is provided to give information about the cfingerd daemon  
  program, and to answer some frequently answered questions.  These are 
  some of the questions I get asked frequently, and some of the questions 
  I figured I should clear up.

* CFINGERD is a free finger daemon replacement for standard finger daemons 
  such as GNU Finger, MIT Finger, or KFINGERD.  CFINGERD is highly   
  becoming a respected standard as the finger daemon to use.  If you are
  unsure about which finger daemon to get, please read over this document 
  before re-enabling your finger daemon!

--

WHAT IS FINGER?

   FINGER is a program that was originally created by MIT to offer user
   information about someone on another machine somewhere else on the
   Internet.  The protocol is defined in RFC 1228.  Finger provides
   information about who you are, what you do, when you last logged in,
   when you last read mail, and many more features.  Unfortunately,
   however, many sites turn off the finger daemon because it's a security
   risk.  That's unfortunate, as the finger program is very valuable, and
   very helpful for friends and other users on the Internet.


WHAT WILL CFINGERD PROVIDE OVER NORMAL FINGER?

   CFINGERD is a program that provides a nicely formatted user
   information display.  CFINGERD was programmed with to help System
   Administrator with improving security while not disabling the
   finger service. Many sites will receive a root finger before they
   are attacked.  With this finger program, you can now turn off the
   ability to have root fingered by adding a ".nofinger" file, or a
   file that doesn't allow for fingering of that user (or anonymity).
   It also is highly configurable when it comes to the question what
   user info to display.  In two levels - first the Administrator
   determines cfingerd's behaviour along with display configuration,
   then users may change things concerning displaying their info, as
   much of it as the Administrator lets them.

   Aside from being security conscious, CFINGERD also offers a great deal
   of other features that normal finger just couldn't provide.  Among
   these are the ability emulate users with scripts, log any finger
   requests either by user or globally, offer custom finger services,
   display header and footer advertisements, and a nicely formatted
   user display to name a few.

   Not only this, but cfingerd allows you to provide fingerable services,
   which allows for users outside of your system to find out more information
   about things you provide; such as information about your ISP, your rates,
   or other things of this nature.  CFINGERD's fingerable services also
   provide the ability to run scripts to display extra information, such as
   a traceroute outside of your area.  The possibilities are endless.

   Of course, you could get GNU's CFINGERD, which is ten times larger,
   harder to configure, and requires more work to do what you could do with
   CFINGERD in just five minutes.  Read over this FAQ.  After you're done,
   read the "README" file, and follow the directions.


WHERE CAN I GET CFINGERD?

   CFINGERD's main archive is available on ftp.infodrom.north.de and
   may be downloaded from the /pub/people/joey/cfingerd/ directory.
   You will want to check this directory on a periodic basis if any
   announcements are given regarding program updates.  Non-official
   updates (or BETA versions) are also available on this site.  You
   may want to check the site once a week or so to check on new
   updates.  Usually, the betas are in testing, and will not be given
   tech support, so be warned.

   The alternative is to get CFINGERD from metalab.unc.edu in the
   directory /pub/Linux/system/Network/finger.  This is the main
   upload site for any updates after they become official.

   Since the current maintainer of cfingerd is also a member of the
   Debian Project many patches are available in the Debian version of
   cfingerd before the next official version of cfingerd is released.
   The diff file can be found on the same FTP site in
   /pub/people/joey/debian/ .


WHO MAINTAINS CFINGERD?

   The cfingerd program was started by Ken Hollis <khollis@bitgate.com>
   who has done a great job implementing this daemon and maintaining
   it.  From 1998 on he decided to work on other projects and handed
   maintainerhip over to Martin Schulze <joey@infodrom.north.de> who has
   worked on other projects before.


IS THERE A MAILING LIST FOR UPDATES?

   Joey has set up a mailing list cfingerd@infodrom.north.de to provide
   a forum for discussions about cfingerd.  If you want to share new ideas
   or patches please contact this list.  You probably want to subscribe
   yourself by sending a mail to majordomo@infodrom.north.de with
   "subscribe cfingerd" in the mail body.


HOW DO I INSTALL CFINGERD ONCE I GET IT?

   The answer is simple.  Simply type "Configure" and answer the questions
   the configuration script prompts you for.  Once that's done, simply
   edit the necessary files, or type "make all" and you will be on your way
   to a complete install of cfingerd.  You will also need to add a line to
   inetd.conf (or at least change one.)  Those instructions are in the
   included "README" file with the standard distribution.


WHAT OPERATING SYSTEMS ARE COMPATIBLE WITH CFINGERD?

   Currently, only Linux, BSD and Solaris are supported.  If you use
   another flavour of Unix, and know enough about C and the system to
   provide a patch, please contact the list mentioned above.


I HAVE A PATCH - HOW DO I SUBMIT THE PATCH?

   Patches are always appreciated.  This is how free software works
   and gets developed.  If you want to submit a patch for inclusion in
   the next release please send the patch with a detailed description
   to the list mentioned above.

   Please keep in mind that a patch can only be applied if the
   maintainer of cfingerd is able to understand it.  Please ensure to
   tell us on which version your patch is based so we have a chance to
   apply it.


WHAT ABOUT A WEB PAGE?

   There will be a web page in the future, but it won't be able to
   provide more information than files within the source tarball.

--

Commonly asked questions:

Q. WHY DOES THIS PROGRAM RUN AS ROOT???

A. Although it may not make sense to you at first glance, the daemon runs as
   root for many security reasons.  First off, the cfingerd.conf file itself
   should be root read-only, so other users can't see how you have cfingerd
   set up.  Secondly, in order for cfingerd to change to the User ID and
   Group ID of the user someone fingered, it must run as root.  If you run
   it as nobody, and finger someone with their home directory of mode 700,
   you will NOT be able to read the .plan or .project files, REGARDLESS.
   Don't worry, though.  The Configure script locates your nobody UID/GID
   automatically, and uses this whenever it performs most of its work.  It
   also executes programs as nobody or as user to ensure total security.
   However in order not to allow random programs being executed by a finger
   procedure you should turn -ALLOW_EXECUTION off in the internal
   configuration section in the cfingerd.conf file.

   Also, keep in mind that all of the UID/GIDs get changed to the NOBODY
   UID/GID *immediately* after file opens, executions of programs, or
   whatever else.  NO PROGRAMS ARE STARTED OR READ AS ROOT!
                   """"""""""""""""""""""""""""""""""""""""

Q. I've seen a patch laying around known as "cfingerd-1.3.0-noroot".
   Should I use this instead of this version?

A. You can if you want, I'm not stopping you.  :) Actually, the reason
   this file was released was because the security holes in cfingerd
   were not completely removed.  (No program is perfect; take
   sendmail's security holes for instance.)  These are addressed in
   the release 1.3.1.  The new version 1.4.0 is even more pickier
   about security and user IDs.


Q. Do you need to have cfingerd run with tcpd?

A. No.  RFC1413 (Host identification) has been put into CFINGERD as an
   accepted standard.  RFC1413 does not provide security for your
   system, but it also identifies who's accessing your system at a
   given time.  Since RFC1413 support is internal, there's no reason
   to run the tcpd wrapper around the program.  Think of it as an
   extra security measure.

   However, if you want to be able to restrict access to your finger
   service to certain addresses or want to keep some others off you'll
   need to run it through the tcpd wrapper.  The standalone version of
   CFINGERD will also make use of it.


Q. When someone fingers my machine, it loops around and keeps spawning finger
   processes on my machine!  Help!

A. The reason this is happening is most likely because you have a finger
   forward that forwards to another system which is pointing back to your
   own originating system.  This will cause an endless loop.  CFINGERD
   will fix this in a later revision (hopefully.)

   Another cause could be that you've got multiple finger list sites that
   are pointing to each other.  For instance, you set up one finger site
   on one system, and another on one other computer.  On computer "A", it
   points to "foo.com", and "foo.com" points to "bleah.com".  Well, since
   they will both ask the other one if they don't know the answer,
   they will keep looping and looping, and looping.  The way to stop
   this is to set system_list_sites to one entry, and make that entry
   "localhost".

   There are many solutions to this problem.  The best one that comes to
   mind may be the fact that you have ALLOW_FINGER_FORWARDING turned on,
   and no entries in the finger_forward listing of hosts.  Having an
   entry of localhost alone in this section will make the processes spawn
   over-and-over again.


Q. When a site fingers my system, I get a syslog entry that says
   "unknown@alarm.signal".

A. This simply means that the site that fingered your system failed to 
   respond to an RFC1413 client query, and thusly timed out, returning
   a standard unknown response.


Q. When someone fingers my system, I get "illegal character in username".

A. Their finger program may be sending a "-L" or a "/W" command when
   fingering the system.  Older versions of cfingerd did not support
   the long formatted display of normal GNU finger.  Cfingerd now
   simply ignores this, to conform with RFC1288.  The remedy is thus
   to get the latest patch to cfingerd, currently available as a
   Debian package.


Q. My header files aren't being displayed properly.  cfingerd's not
   intercepting the commands.

A. Try adding the line "+ALLOW_LINE_PARSING" to your internal_config
   section.  That usually helps.


Q. The no-name banner isn't showing, and I have it set to true!

A. Make sure system_list is set to FALSE for remote systems.  If it's not,
   the SYSTEM_LIST variable (if set to true) will override the
   NO_NAME_BANNER option.


Q. I have a big site, and I'm trying to list users that are on that site!
   It's not working, and I'm sure I've set it up correctly!

A. Make sure that "+ALLOW_USERLIST_ONLY" is set in the internal_config
   section.  Also, make sure that "localhost" is the last entry in the
   system_list_sites configuration section.  If it's placed anywhere else
   in that section, it will stop at that entry.  "localhost" is considered
   to be the ending entry.


Q. When I finger someone on the system, it says "This daemon must be run as
   root!"

A. The way to fix this is to change the inetd.conf entry from "daemon" to
   "root" permissions.  This can be done by using the following entry in 
   your inetd.conf file:

finger  stream  tcp     nowait  root    /usr/sbin/tcpd  /usr/sbin/in.cfingerd

   Save that, then reboot your inetd program.  (This can be done by typing
   "killall -HUP inetd").  Then, finger away!


Q. If I have a bug, what do I do?

A. We have received messages that simply have the entry "SIGSEGV
   Received!" and nothing else was sent.  This probably is not helpful
   for finding the real source of the problem.

   We have had lots of people modifying the cfingerd.conf file, adding
   spaces here and there, or tabs here and there.  DO NOT change the
   format, only the ASCII chars.  DO NOT SUBSTITUTE SPACES FOR TABS.  If
   all else fails, backup your cfingerd.conf, re-copy the original
   cfingerd.conf file into your /etc directory and apply all changes
   you've made before watching what happens.  Chances are, something got
   messed up during the modification.

   If you expect to get a helping hand from other cfingerd users or its
   maintainers, please at least attach your cfingerd.conf file.


Q. I see ".fingerlog (Operation not permitted)" in my syslog!  What now?

A. Most likely, you were running an older version of cfingerd that had
   root access to that file.  Former versions of cfingerd didn't change
   the effective user id to the user before accessing the logfile.  To
   fix this, type: "chown user.group .fingerlog", where user.group is
   whoever has that directory as home directory, and all will be well.
