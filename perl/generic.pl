## Generic/Standard routines
## by Ken Hollis <khollis@bitgate.com>
##
## This program is GPLed.  Please read "LICENSE" for more information.
## Copyright (C) 1996, Bitgate Software.

sub get_os {
	local ($uname);
	local ($ver);

	print "Operating system ... ";

	$uname = `uname`;
	chop($uname);
	$uname =~ tr/a-z/A-Z/;

	$ver = `uname -r`;
	chop($ver);

	print "$uname\n";
	return ($uname,$ver);
}

sub get_lastlog {
	my $ll = shift;

	print "Lastlog ... ";

	if (!$ll) {
	    if (-e "/var/log/lastlog") {
		$ll = "/var/log/lastlog";
	    } elsif (-e "/var/adm/lastlog") {
		$ll = "/var/adm/lastlog";
	    } else {
		$ll = "/var/adm/wtmp";
	    }
	}

	print "$ll\n";
	return $ll;
}

sub get_nobody {
	my $uid = shift;
	my $gid = shift;
	my @foo;

	print "Nobody UID/GID ... ";

	if (($uid eq "") || ($gid eq "")) {
	    if (open (PW, "/etc/passwd")) {
		while (<PW>) {
		    next unless (/^nobody:/);
		    @foo = split (/:/);
		    $uid = $foo[2] if (!$uid);
		    $gid = $foo[3] if (!$gid);
		}
		close (PW);
	    }
	}

	if (($uid eq "") || ($gid eq "")) {
		$uid = 65535;
		$gid = 65535;
	}

	print "$uid, $gid\n";
	return "$uid,$gid";
}

sub has_shadow {
	local($shad);

	print "Shadow passwords ... ";

	if (-e "/etc/shadow") {
		$shad = "Y";
	} else {
		$shad = "N";
	}

	print (($shad eq "Y") ? "/etc/shadow\n" : "No shadow\n");
	return $shad;
}

sub get_mailspool {
	my $spool = shift;

	print "Mail spool directory ... ";
	if (!$spool) {
	    if (-d "/var/spool/mail") {
		$spool = "/var/spool/mail";
	    } elsif (-d "/var/mail") {
		$spool = "/var/mail";
	    } else {
		print "Not found.  Assuming ";
		$spool = "/var/spool/mail";
	    }
	}
	print "$spool\n";
	return $spool;
}

sub get_mandir {
	my $dir = shift;

	print "Manpages root directory ... ";
	if (!$dir) {
	    if (-d "/usr/share/man") {
		$dir = "/usr/share/man";
	    } elsif (-d "/usr/man") {
		$dir = "/usr/man";
	    } else {
		print "Not found.  Assuming ";
		$dir = "/usr/man";
	    }
	}
	print "$dir\n";
	return $dir;
}

1;
