/*
 * CFINGERD
 * RFC1413 implementation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 1, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "cfingerd.h"

void rfc1413_alarm(int signal)
{
    if (signal == SIGALRM)
	ident_user = "unknown@alarm.signal";
}

/* Self contained RFC1413 implementation.  Thanks to Joel Katz for parts of
 * the implementation.  Completely rewritten by yours truly to be self-
 * contained in a single program.  Simple, easy to use.
 */
#define BUFLEN	256
char *get_rfc1413_data( struct sockaddr_in local_addr )
{
    int i, j;
    struct sockaddr_in sin;
    char buffer[1024], buf[BUFLEN], uname[64], *bleah;
    char *cp, *xp;
    struct servent *serv;

    bleah = (char *) malloc(BUFLEN);
    memset(bleah, 0, BUFLEN);

    j = socket(AF_INET, SOCK_STREAM, 0);
    if (j < 2) {
	snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	syslog(LOG_ERR, "rfc1413-socket: %s", strerror(errno));
	return(bleah);
    }

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = local_addr.sin_addr.s_addr;
    sin.sin_port = 0;
    i = bind(j, (struct sockaddr *) &sin, sizeof(sin));
    if (i < 0) {
	snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	syslog(LOG_ERR, "rfc1413-socket: %s", strerror(errno));
	return(bleah);
    }

    sin.sin_family = AF_INET;
    if ((serv = getservbyname("auth","tcp")) != NULL)
	sin.sin_port = serv->s_port;
    else
	sin.sin_port = htons(113);
    sin.sin_addr.s_addr = inet_addr(ip_address);
    signal(SIGALRM, rfc1413_alarm);
    alarm(5);

    i = connect(j, (struct sockaddr *) &sin, sizeof(sin));
    if (i < 0) {
	syslog(LOG_ERR, "rfc1413-connect: %s", strerror(errno));
	close(j);
	snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	alarm(0);
	return(bleah);
    }

    snprintf(buffer, sizeof(buffer), "%d,%d\n", remote_port, local_port);
    write(j, buffer, strlen(buffer));

    memset(buf, 0, sizeof(buf));
    if (read(j, buf, 256) <= 0) {
	snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
    } else {
	/*
	 * The response should look like
	 *   23640 , 60179 : USERID : UNIX : joey^M
	 */
	if ((cp = strchr(buf, ':')) == NULL)
	    snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	else if ((cp = strchr(++cp, ':')) == NULL)
	    snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	else if ((cp = strchr(++cp, ':')) == NULL)
	    snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);

	if (!cp) {
	    close(j);
	    alarm(0);
	    return (bleah);
	}

	if (*(++cp) == ' ') cp++;
	memset(uname, 0, sizeof(uname));
	for (xp=uname; *cp != '\0' && *cp!='\r'&&*cp!='\n'&&strlen(uname)<sizeof(uname); cp++)
	    *(xp++) = *cp;

	if (!strlen(uname)) {
	    close(j);
	    snprintf(bleah, BUFLEN, "unknown@%s", remote_addr);
	    alarm(0);
	    return(bleah);
	}

	snprintf(bleah, BUFLEN, "%s@%s", uname, remote_addr);
    }

    alarm(0);
    return(bleah);
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  tab-width: 8
 * End:
 */
