#ifndef __GETUTENT_H__
#define __GETUTENT_H__

#ifdef BSD
struct utmp *getutent();  /* returns next utmp file entry   */
void setutent();
#ifndef __FreeBSD__
char *getdomainname(char *, size_t);
#endif /* !__FreeBSD__ */
#endif

#endif
